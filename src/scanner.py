#!/usr/bin/env python3
"""Scan local directories for files to upload for amazon-sync"""

import os
import hashlib
import portalocker
import dateutil.parser as parse_date
import json
import ctypes
import time
import bcrypt
import subprocess
from handler import Handler
from sync import Sync
from datetime import datetime
from threading import Thread
from threading import Lock


class Registry:
    """Holds all file info for scanner"""

    class FileGrouping:
        """Holds info on all different versions of a file"""

        class File:
            """Contains information on a particular file"""

            def __init__(self, name, path, file_hash, date, parent, share, encrypted_id=None, encrypted_hash=None):
                """Inits file if date is not passed current date is used"""
                # locks individual file object
                self.lock = Lock()
                # name of file
                self.name = name
                # share file is in
                self.share = share
                # relative path to file
                self.path = path
                # hash of file
                self.hash = file_hash
                # hash of encrypted file
                self.encrypted_hash = encrypted_hash
                # date this file version was found
                self.updated = date
                if encrypted_id is None:
                    sha256 = hashlib.sha256()
                    sha256.update(path.encode('utf-8'))
                    sha256.update(file_hash.encode('utf-8'))
                    sha256.update(share.encode('utf-8'))
                    # add a random salt + password to ensure random encrypted ID's
                    sha256.update(bcrypt.hashpw(parent.key.encode(), bcrypt.gensalt(12)))
                    # encrypted ID sha256(path + file_hash)
                    self.encrypted_id = sha256.hexdigest()
                else:
                    self.encrypted_id = encrypted_id
                # parent scanner object
                self.parent = parent
                # set up sync obj
                self.sync = Sync(self.parent.remote)

            def to_json(self):
                """Converts obj to json serialized string"""
                packed = json.dumps(dict(
                    name=self.name,
                    share=self.share,
                    path=self.path,
                    hash=self.hash,
                    encrypted_hash=self.encrypted_hash,
                    updated=self.updated.isoformat(),
                    encrypted_id=self.encrypted_id
                ))
                return packed

            def encrypt(self):
                """Encrypts the file and writes it to staging"""
                # setup crypt.so
                libcrypt_path = os.path.join("..", "libCrypt.so")
                self.lock.acquire()
                crypt = ctypes.cdll.LoadLibrary(libcrypt_path)
                encrypt_file = crypt.encrypt_file
                encrypt_file.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p]
                dest = os.path.join(self.parent.staging, self.encrypted_id)
                self.lock.release()

                # check total crypt jobs
                while True:
                    self.parent.lock.acquire()
                    if self.parent.total_crypt_jobs < self.parent.max_crypt_jobs:
                        break
                    else:
                        self.parent.lock.release()
                        time.sleep(5)
                self.parent.total_crypt_jobs += 1
                self.parent.lock.release()

                # relative path -> full path
                self.lock.acquire()
                path = os.path.join(self.parent.dirs[self.share].path, self.path)
                # grab key as well
                key = self.parent.key
                self.lock.release()

                print("[*] Encrypting", path)
                # start encryption
                encrypt_file(key.encode(),
                             path.encode(),
                             dest.encode())
                # get/set encrypted hash
                self.lock.acquire()
                self.encrypted_hash = self.parent.hash_file(dest)
                self.lock.release()

                # crypt job is done
                # grab lock, dec total, drop lock
                self.parent.lock.acquire()
                self.parent.total_crypt_jobs -= 1
                self.parent.lock.release()
                print("[*] Finished encrypting", path)
                return

            def decrypt(self, dest=None):
                """Decrypts and saves the file to the destination"""
                # set path of encrypted file
                path = os.path.join(self.parent.staging, self.encrypted_id)

                # set destination path
                if dest is None:
                    dest = os.path.join(self.parent.dirs[self.share].path, self.path)
                print("[*] Decrypting", path, "to", dest)

                # set path to libcrypt.so
                libcrypt_path = os.path.join("..", "libCrypt.so")
                self.lock.acquire()
                crypt = ctypes.cdll.LoadLibrary(libcrypt_path)
                decrypt_file = crypt.decrypt_file
                decrypt_file.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p]
                self.lock.release()

                # check total crypt jobs
                while True:
                    self.parent.lock.acquire()
                    if self.parent.total_crypt_jobs < self.parent.max_crypt_jobs:
                        break
                    else:
                        self.parent.lock.release()
                        time.sleep(5)
                self.parent.total_crypt_jobs += 1
                self.parent.lock.release()

                # relative path -> full path
                self.lock.acquire()
                dest = os.path.join(self.parent.dirs[self.share].path, dest)
                # grab key as well
                key = self.parent.key
                self.lock.release()
                # start encryption
                decrypt_file(key.encode(),
                             path.encode(),
                             dest.encode())

                # crypt job is done
                # grab lock, dec total, drop lock
                self.parent.lock.acquire()
                self.parent.total_crypt_jobs -= 1
                self.parent.lock.release()
                print("[*] Finished Decrypting", path)
                return

        def __init__(self, parent):
            """Inits file grouping"""
            # obj lock
            self.lock = Lock()
            # dict of all version of file
            # key is file_hash
            self.versions = {}
            # parent scanner object
            self.parent = parent

        def to_json(self):
            """Converts obj to json serialized string"""
            packed = {}
            for key in self.versions.keys():
                packed[key] = self.versions[key].to_json()
            packed = json.dumps(packed)
            return packed

        def new_version(self, name, path, file_hash, share, date=datetime.now()):
            """adds a new version of a file to the version dict"""
            self.lock.acquire()
            # build targ
            targ = share + "_" + file_hash
            self.versions[targ] = self.File(name, path, file_hash, date, self.parent, share)
            self.lock.release()
            # encrypt file
            self.versions[targ].encrypt()
            # upload file
            version_targ = share + "_" + file_hash
            src = os.path.join(self.parent.staging, self.versions[version_targ].encrypted_id)
            self.versions[targ].sync.upload(src, self.versions[version_targ].encrypted_id, self.versions[version_targ])
            # if upload successful remove src file in staging
            os.remove(src)
            #self.versions[file_hash].sync.download(self.versions[file_hash].encrypted_id, src, self.versions[file_hash])
            # if download successful decrypt
            #self.versions[file_hash].decrypt()
            return

        def check_grouping(self, name, path, file_hash, share, date=datetime.now()):
            """Checks to see if a version of a file is present in the version table"""
            self.lock.acquire()
            targ = share + "_" + file_hash
            if targ in self.versions.keys():
                # version is present in versions
                #print("Version already logged")
                self.lock.release()
                return
            else:
                # version not logged previously
                print("[*] New version found of", path)
                self.lock.release()
                self.new_version(name, path, file_hash, share, date)
                return

    def __init__(self, parent):
        """Inits registry"""
        # object lock
        self.lock = Lock()
        # dict of files in registry
        # key is file path
        self.files = {}
        # parent object
        self.parent = parent
        return

    def to_json(self):
        """Converts obj to json serialized string"""
        packed = {}
        for key in self.files.keys():
            packed[key] = self.files[key].to_json()
        packed = json.dumps(packed)
        return packed


    def get_json(self):
        """Converts obj to json serialized string if Amazon-Sync is setup"""
        if self.parent.key is None and self.parent.staging is None:
            packed = "setup"
        else:
            packed = {}
            for key in self.files.keys():
                packed[key] = self.files[key].to_json()
            packed = json.dumps(packed)
        return packed


    def save(self):
        """Saves registry"""
        packed = self.to_json()
        fp = open("registry", "w+")
        fp.write(packed)
        fp.close()
        return

    def load_json(self, packed):
        """Loads to the registry from a packed json"""
        packed = json.loads(packed)
        for file in packed.keys():
            packed_file = json.loads(packed[file])
            self.files[file] = self.FileGrouping(self.parent)
            for version in packed_file.keys():
                packed_version = json.loads(packed_file[version])
                self.files[file].versions[version] = self.files[file].File(
                    packed_version["name"],
                    packed_version["path"],
                    packed_version["hash"],
                    parse_date.parse(packed_version["updated"]),
                    self.parent,
                    packed_version["share"],
                    packed_version["encrypted_id"],
                    packed_version["encrypted_hash"]
                )
        return

    def load(self):
        """Loads the registry"""
        # check to make sure registry.dat exists
        if os.path.isfile("registry"):
            print("[*] Loading registry")
            fp = open("registry", "r")
            packed = fp.read()
            self.load_json(packed)
        return

    def new_file_grouping(self, name, path, file_hash, share, date=datetime.now()):
        """Adds a new file grouping to the registry"""
        # create new filegrouping obj
        self.lock.acquire()
        self.files[path] = self.FileGrouping(self.parent)
        self.lock.release()

        # add new version of file to grouping
        self.files[path].new_version(name, path, file_hash, date, share)
        return

    def check_reg(self, name, path, file_hash, share, date=datetime.now()):
        """
        Checks if a file is in the registry
        If its not it adds it then starts uploading it
        """
        self.lock.acquire()
        #print(">>path:", path)
        if path in self.files.keys():
            # file in registry
            #print("file in registry")
            self.files[path].check_grouping(name, path, file_hash, share, date)
            self.lock.release()
            return
        else:
            # file not in registry
            # start thread to encrypt/upload
            #print("New file!")
            self.lock.release()
            t = Thread(target=self.new_file_grouping, args=(name, path, file_hash, date, share))
            self.parent.lock.acquire()
            self.parent.worker_threads.append(t)
            self.parent.lock.release()
            t.start()
            return

class Dir_info():
    """Contains on the dirs being watched"""
    def __init__(self, path):
        # path of dir
        self.path = path
        # is dir being scanned currently?
        self.active = False
        # lock
        self.lock = Lock()


class scanner():
    """Scans local directories"""
    def __init__(self, key=None, staging=None):
        """Inits scanner with some defaults"""
        # obj lock
        self.lock = Lock()
        # list of  dir objects
        self.dirs = {}
        # base key used to encrypt every file
        self.key = key
        # directory to store temporary files
        self.staging = staging
        # rclone remote to sync to
        self.remote = None
        # maximum number of scan jobs to run at once
        self.max_scan_jobs = 5
        # current number of total scan jobs running
        self.total_scan_jobs = 0
        # maximum number of encryption/decryption jobs to run at once
        self.max_crypt_jobs = 8
        # number of total encryption/decryption jobs in action
        self.total_crypt_jobs = 0
        # maximum number of upload/download jobs to run at once
        self.max_acd_jobs = 3
        # number of total upload/download jobs in action
        self.total_acd_jobs = 0
        # scanner threads
        self.scanner_threads = []
        # worker threads
        self.worker_threads = []
        # handler object
        self.handler = None
        # handler threads
        self.handler_threads = []
        # file registry
        self.reg = Registry(self)
        # status
        self.status = "running"

    def to_json(self):
        """Converts scanner obj to json"""
        temp_dirs = {}
        for key, value in self.dirs.items():
            temp_dirs[key] = value.path
        packed = json.dumps(dict(
            dirs=temp_dirs,
            key=self.key,
            staging=self.staging,
            remote=self.remote,
            max_scan_jobs=self.max_scan_jobs,
            max_crypt_jobs=self.max_crypt_jobs,
            max_acd_jobs=self.max_acd_jobs
        ))
        return packed

    def save(self):
        """Saves Scanner setttings"""
        packed = self.to_json()
        fp = open("../settings/settings", "w+")
        fp.write(packed)
        fp.close()

    def load(self):
        """Loads scanner object from json"""
        if os.path.isfile("../settings/settings"):
            print("[*] Loading Settings")
            fp = open("../settings/settings", "r")
            packed = fp.read()
            packed = json.loads(packed)
            self.key = packed["key"]
            self.max_scan_jobs = packed["max_scan_jobs"]
            self.max_crypt_jobs = packed["max_crypt_jobs"]
            self.max_acd_jobs = packed["max_acd_jobs"]
            self.staging = packed["staging"]
            for name, path in packed["dirs"].items():
                self.add_dir(path, name)



    def add_dir(self, path, name):
        """Adds a dir for scanner to watch"""
        # grab lock, add dir, drop lock
        print("[*] adding dir", path)
        self.lock.acquire()
        self.dirs[name] = Dir_info(path)
        self.lock.release()
        return

    def hash_file(self, path):
        """Returns the MD5 hash of a file"""
        # open file
        fp = open(path, "rb")
        # locks file exclusively so no other process can open it
        # on linux this does nothing since linux by default uses advisory locking
        # still working on how to fix this ~('_')~
        portalocker.lock(fp, portalocker.LOCK_EX)

        # digest file 4096 at a time
        md5 = hashlib.md5()
        # get size one last time
        size = os.stat(path).st_size
        while(size > 0):
            buff = fp.read(4096)
            md5.update(buff)
            # subtract len of buff not 4096
            size -= len(buff)

        # unlock and close
        portalocker.unlock(fp)
        fp.close()
        return md5.hexdigest()


    def scan_dir(self, name, path):
        """Scans a directory for new/changed files"""
        print("[*] scanning", path)
        # scan dir
        for entry in os.scandir(path):
            if entry.is_dir():
                self.scan_dir(name, entry.path)
            elif entry.is_file():
                file_hash = self.hash_file(entry.path)
                temp_path = os.path.join(path, entry.name)
                #print("    ", entry.name, file_hash)
                temp_path = temp_path.replace(self.dirs[name].path + "/", "")
                self.reg.check_reg(entry.name, temp_path, file_hash, name)
        # set dir to be dormant
        self.dirs[name].lock.acquire()
        self.dirs[name].active = False
        self.dirs[name].lock.release()
        self.lock.acquire()
        self.max_scan_jobs -= 1
        self.lock.release()
        print("[*] Finished scanning", path)
        return


    def start_scan(self):
        """Starts a scan of a root dir"""
        # start a new thread for each dir to watch
        while 1:
            self.lock.acquire()
            for key, cur_dir in self.dirs.items():
                # check if dir is active
                self.dirs[key].lock.acquire()
                if self.dirs[key].active is False:
                    self.dirs[key].active = True
                else:
                    self.dirs[key].lock.release()
                    continue
                self.dirs[key].lock.release()
                t = Thread(target=self.scan_dir, args=(key, cur_dir.path,))
                self.scanner_threads.append(t)
                self.max_scan_jobs += 1
                t.start()
            self.lock.release()
            # wait for scanners to finish before restarting
            self.cull()
            for t in self.scanner_threads:
                t.join()
            # check for exit every minute and restart scans in 30 minutes
            for i in range(0, 30):
                self.lock.acquire()
                if self.status != "running":
                    self.lock.release()
                    return
                self.lock.release()
                time.sleep(5)

    def check_rclone(self):
        """Checks if rclone is setup"""
        # no remote is set configure remote
        if self.remote is None:
            print("[!] Configure Rclone!")
            exit()
        else:
            ret = subprocess.Popen("rclone listremotes", shell=True, stdout=subprocess.PIPE)
            ret.wait()
            remotes = ret.stdout.read()
            remotes = remotes.decode()
            remotes = remotes.split(":\n")
            # check if self.remote is in rclone
            if self.remote in remotes and len(self.remote) > 0:
                sync = Sync(self.remote)
                # create root sync dir if it does not exist at remote
                sync.create_dir(self.remote)
                return True

    def start(self):
        """Starts scanner"""
        print("[*] Starting Amazon-Sync")
        self.load()
        self.reg.load()
        self.check_rclone()
        # start handler
        self.handler = Handler(self)
        t = Thread(target=self.handler.start)
        self.handler_threads.append(t)
        t.start()
        self.start_scan()
        return



    def cull(self):
        """Removes dead threads from thread lists"""
        # scanner threads
        for t in self.scanner_threads:
            # if thread is dead remove it
            if t.isAlive() is False:
                self.scanner_threads.remove(t)

        # worker threads
        for t in self.worker_threads:
            # if thread is dead remove it
            if t.isAlive() is False:
                self.worker_threads.remove(t)

        # handler threads
        for t in self.handler_threads:
            # if thread is dead remove it
            if t.isAlive() is False:
                self.handler_threads.remove(t)
        return

    def exit(self):
        print("[*] exiting Amazon-Sync")
        self.status = "exit"
        self.cull()
        # wait for scanner threads to complete
        for t in self.scanner_threads:
            t.join()
        # wait for worke threads to complete
        for t in self.worker_threads:
            t.join()
        for t in self.handler_threads:
            t.join()
        time.sleep(5)
        self.cull()
        self.reg.save()
        self.save()
        exit()



def main():
    #print("adding test_dir")
    #scan = scanner("wootywoot123456", "/mnt/435B00F369EE2339/Documents/Projects/Amazon-Sync/staging")
    scan = scanner()
    #scan.add_dir("/mnt/435B00F369EE2339/Documents/Projects/Amazon-Sync/test_dir", "test_dir")
    #scan.add_dir("/mnt/435B00F369EE2339/Documents/Projects/Amazon-Sync/test_dir 2", "test_dir 2")
    scan.remote = "amazon_sync"
    scan.start()


if __name__ == '__main__':
    main()
