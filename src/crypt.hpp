#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include "cryptopp/modes.h"
#include "cryptopp/aes.h"
using CryptoPP::AES;
#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;
#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::AuthenticatedEncryptionFilter;
using CryptoPP::AuthenticatedDecryptionFilter;
#include <string>
#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;
#include "cryptopp/cryptlib.h"
using CryptoPP::BufferedTransformation;
using CryptoPP::AuthenticatedSymmetricCipher;
#include "cryptopp/gcm.h"
using CryptoPP::GCM;
using CryptoPP::GCM_TablesOption;
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/sha.h"
#include "cryptopp/base64.h"
#include "cryptopp/files.h"

#include "assert.h"

//include bcrypt
#include "../libs/bcrypt/bcrypt.h"

#define TAG_SIZE 16
#define FILE_READ_SIZE 1024

class crypt{
private:
    //path file is located at
    std::string path;
    //temp variables to encrypt/decrypt
    byte key[ CryptoPP::AES::MAX_KEYLENGTH ];
    byte iv[ CryptoPP::AES::BLOCKSIZE ];
    char salt[BCRYPT_HASHSIZE];

    //generates a hash using SHA256
    std::string sha256(std::string);
    //wipes key/iv
    void wipe();

public:
    crypt();
    //Sets a new key and generates a salt
    void newkey(std::string new_key);
    //sets an already key (doesn't gen a salt)
    void setkey(std::string key);
    //sets the iv from a string
    void set_iv_from_string(std::string data);
    //returns the iv as a string
    std::string get_iv_as_string();
    //returns the salt as a string
    std::string get_salt_as_string();
    //sets path to file to encrypt
    //void set_path(std::string target);
    //encrypts a file
    void encrypt_file(std::string src, std::string dest);
    //decrypts a file
    void decrypt_file(std::string src, std::string dest, std::string key, crypt* cryptobj);

    // Individual Encryption methods
    //encrypts with aes cbc
    std::string encrypt_cbc(std::string plaintext);
    //encrypts with aes gcm
    std::string encrypt_gcm(std::string plaintext);
    //decrypts with aes cbc
    std::string decrypt_cbc(std::string ciphertext);
    //decrypts with aes gcm
    std::string decrypt_gcm(std::string cipher);
};

//wrappers for python
//encrypts a file
//returns a salt
extern "C" {
    void encrypt_file(const char* new_key, const char* path, const char* dest);
    void decrypt_file(const char* new_key, const char* path, const char* dest);
    void free_string(const char* targ);
}


/*extern "C" {
    crypt_wrapper* new_crypt() { return new crypt_wrapper(); }
    //void new_key(crypt_wrapper* crypt_wrapper) { crypt_wrapper->newkey();}
    void print(crypt_wrapper* crypt_wrapper, const char* data) { crypt_wrapper->print(data); }
    void wrap_test_print(const char* data) {test_print(data);}
}*/


