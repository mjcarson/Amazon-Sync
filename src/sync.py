#!/usr/bin/env python3
"""Uploads/Downloads files to Amazon Cloud"""
import subprocess
import os
import time

class Sync():
    """Uploads/Downloads files to Amazon Cloud"""
    def __init__(self, remote):
        """Inits sync"""
        self.remote = remote

    def create_dir(self, path):
        """Creates a dir on Amazon Cloud"""
        # set cmd
        cmd = "rclone mkdir " + self.remote + ":" + path
        # create dir
        ps = subprocess.Popen(cmd, shell=True)
        ps.wait()

    def upload(self, src, dest, parent=None):
        """Uploads local files to Amazon Cloud"""
        # set remote path
        dest = os.path.join(self.remote, dest)

        # set cmd
        print("[*] Uploading", src)
        cmd = "rclone copyto --no-traverse " + src + " " + self.remote + ":" + dest
        # upload local file
        ps = subprocess.Popen(cmd, shell=True)
        ps.wait()
        print("[*] Finished uploading", src)
        time.sleep(15)
        return

    def download(self, src, dest, parent=None):
        """Downlads files from Amazon Cloud"""
        # set remote path
        src = os.path.join(self.remote, src)

        # set cmd
        print("[*] Downloading", src)
        cmd = "rclone copyto --no-traverse " + self.remote + ":" + src + " " + dest
        # upload local file
        ps = subprocess.Popen(cmd, shell=True)
        ps.wait()
        print("[*] Finished downloading", src)
        return

    def check_hash(self, targ, local_hash):
        """Checks a remote file against a local md5sum"""
        # set cmd
        cmd = "rclone md5sum " + self.remote + ":" + targ
        # run cmd
        ret = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        ret.wait()
        remote_hash = ret.stdout.read()
        # parse return value
        remote_hash = remote_hash.decode()
        remote_hash = remote_hash.split()
        remote_hash = remote_hash[0]
        # check remote vs local hash
        if remote_hash == local_hash:
            return True
        return False
