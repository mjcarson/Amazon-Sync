#!/usr/bin/env python3
import socket
import time
import os
from threading import Thread
from select import select
from threading import Lock

class Handler():
    """Handles communications between django and Amazon-Sync"""
    def __init__(self, parent):
        """Inits handler"""
        # socket handler will listen on
        self.listener = None
        # parent of handler
        self.parent = parent
        # status of handler
        self.status = "Running"
        # processes working requests
        self.workers = []
        # Lock
        self.lock = Lock()

    def start(self):
        """Start handler"""
        # start listening
        t = Thread(target=self.__listen, args=("127.0.0.1", 8099))
        t.start()

    def __listen(self, ip, port):
        """Listens for conns from django"""
        print("[*] Listening on %s:%d" % (ip, port))
        self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listener.bind((ip, port))
        read_socks = [self.listener]
        self.listener.listen(4)
        # set loc status
        status = "running"
        while status != "exit":
            self.__cull()
            readable, _, _ = select(read_socks, [], [], 10)
            for sock in readable:
                client, addr = self.listener.accept()
                #print("%s connected" % str(addr))
                # start worker thread
                t = Thread(target=self.__handle, args=(client, addr))
                t.start()
                self.workers.append(t)
            self.lock.acquire()
            if self.status == "exit":
                status = "exit"
            self.lock.release()
        self.listener.close()
        self.exit_sync()

    def __send_regs(self, client):
        """Sends regs to client in a packed string"""
        # get json
        packed = self.parent.reg.get_json()
        # send length of json
        client.send(str(len(packed)).encode())
        time.sleep(.1)
        # send json
        client.send(packed.encode())
        return

    def __scan_dir(self, name, path):
        """Starts scan of a dir"""
        # start scan of dir
        self.parent.dirs[name].lock.acquire()
        if self.parent.dirs[name].active is False:
            self.parent.dirs[name].active = True
        else:
            self.parent.dirs[name].lock.release()
            return
        self.parent.dirs[name].lock.release()
        t = Thread(target=self.parent.scan_dir, args=(name, path,))
        self.parent.scanner_threads.append(t)
        self.parent.max_scan_jobs += 1
        t.start()
        return

    def __download_file(self, share, file, path, version, encrypted_id):
        """calls sync download then decrypts that downloaded file"""
        # build target dest
        dest = os.path.join(self.parent.staging, encrypted_id)
        # download file
        self.parent.reg.files[file].versions[version].sync.download(encrypted_id, dest)
        # decrypt file
        self.parent.reg.files[file].versions[version].decrypt()

    def __handle(self, client, addr):
        """Handles one connection to Django"""
        try:
            #print("handling %s" % str(addr))
            order = client.recv(2048).decode()
            # grab regs
            if order == "get_reg":
                self.__send_regs(client)
            # setup key/staging
            elif order == "setup":
                # set key
                self.parent.key = client.recv(1024).decode()
                # set staging
                self.parent.staging = client.recv(1024).decode()
            # add new dir to watch
            elif order == "add_dir":
                # get name
                name = client.recv(1024).decode()
                # get path
                path = client.recv(1024).decode()
                # add dir
                self.parent.add_dir(path, name)
                # scan dir
                self.__scan_dir(name, path)
            # download file
            elif order == "download_file":
                # get share
                share = client.recv(1024).decode()
                # get file
                file = client.recv(1024).decode()
                # get path
                path = client.recv(1024).decode()
                # get version
                version = client.recv(1024).decode()
                # get encrypted_id
                encrypted_id = client.recv(1024).decode()
                # download file
                self.__download_file(share, file, path, version, encrypted_id)
            # shutdown
            elif order == "shutdown":
                # shutdown listener
                self.lock.acquire()
                self.status = "exit"
                self.lock.release()
        except Exception as err:
            print("[!] Error: %s" % err)
        client.close()

    def __cull(self):
        """Removes dead threads from thread lists"""
        # worker threads
        for t in self.workers:
            # if thread is dead remove it
            if t.isAlive() is False:
                self.workers.remove(t)
        return

    def exit_sync(self):
        """Exits Handler"""
        for worker in self.workers:
            worker.join()
        self.parent.exit()
        return

#handler = Handler()
#time.sleep(15)
#handler.exit_sync()
