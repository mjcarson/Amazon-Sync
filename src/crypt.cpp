#include "crypt.hpp"

////////////////////////////////
//// Crypt Class
////////////////////////////////
// initializes the crypt class
// key will be initialized to 0
// TODO: IV will be randomly generated
crypt::crypt(){
    int i;
    char temp[AES::BLOCKSIZE];
    AutoSeededRandomPool rnd;

    //generates the IV
    rnd.GenerateBlock(iv, AES::BLOCKSIZE);
}

////////////////////////////////
//// utility funcs
////////////////////////////////

//returns a sha256 hash
std::string crypt::sha256(std::string aString){
    std::string digest;
    CryptoPP::SHA256 hash;
    CryptoPP::StringSource foo(aString, true, new CryptoPP::HashFilter(hash, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(digest))));
    return digest;
}

//sets a new key and generates a salt
void crypt::newkey(std::string new_key) {
    int i;
    char hash[BCRYPT_HASHSIZE];
    std::string temp;

    //generate the salt
    bcrypt_gensalt(15, salt);

    //hash the password + salt
    bcrypt_hashpw(new_key.c_str(), salt, hash);
    temp = sha256(std::string(hash));
    for (i = 0; i < AES::MAX_KEYLENGTH; i++) {
        key[i] = temp.c_str()[i];
    }

}

//sets a new key and generates a salt
void crypt::setkey(std::string new_key) {
    int i;
    char hash[BCRYPT_HASHSIZE];
    std::string temp;

    //hash the password + salt
    bcrypt_hashpw(new_key.c_str(), salt, hash);
    temp = sha256(std::string(hash));
    for (i = 0; i < AES::MAX_KEYLENGTH; i++) {
        key[i] = temp.c_str()[i];
    }

}

//returns the iv as a string
std::string crypt::get_iv_as_string(){
    std::string temp(reinterpret_cast<char*>(iv), sizeof(iv));
    return temp;
}

//sets the iv from a string
void crypt::set_iv_from_string(std::string data){
    int i;
    for (i = 0; i < AES::BLOCKSIZE; i++) {
        iv[i] = data.c_str()[i];
    }
}

//returns the salt as a string
std::string crypt::get_salt_as_string() {
    return std::string(salt);
}

//wipes key/iv
void crypt::wipe() {
    memset(key, 0x00, sizeof(CryptoPP::AES::MAX_KEYLENGTH));
    memset(iv, 0x00, sizeof(AES::BLOCKSIZE));
}

////////////////////////////////
//// encryption/decrytion funcs
////////////////////////////////
//GCM is the default to be used
//It is very possible only GCM will be supported for a while ~('_')~

//encrypts with aes cbc
std::string crypt::encrypt_cbc(std::string plaintext) {
    std::string ciphertext;
    std::cout << "Plain Text (" << plaintext.size() << " bytes)" << std::endl;
    std::cout << plaintext;
    CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::MAX_KEYLENGTH );
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

    CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
    stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
    stfEncryptor.MessageEnd();
    //std::cout << ciphertext << std::endl;
    std::cout << decrypt_cbc(ciphertext) << std::endl;
    std::cout << "------------------------" << std::endl << std::endl;
    return ciphertext;
}

//encrypts with aes gcm
std::string crypt::encrypt_gcm(std::string plaintext) {
    //std::cout << "------------encryption----------" << std::endl;


    std::string adata = "wootywoot";


    // Encrypted, with Tag
    std::string cipher, encoded;

    // Recovered
    std::string radata, rpdata;

    /*********************************\
    \*********************************/

    try
    {
        GCM< AES >::Encryption e;
        e.SetKeyWithIV( key, sizeof(key), iv, sizeof(iv) );
        // Not required for GCM mode (but required for CCM mode)
        // e.SpecifyDataLengths( adata.size(), plaintext.size(), 0 );

        AuthenticatedEncryptionFilter ef( e,
                                          new StringSink( cipher ), false, TAG_SIZE
        ); // AuthenticatedEncryptionFilter

        // AuthenticatedEncryptionFilter::ChannelPut
        //  defines two channels: "" (empty) and "AAD"
        //   channel "" is encrypted and authenticated
        //   channel "AAD" is authenticated
        ef.ChannelPut( "AAD", (const byte*)adata.data(), adata.size() );
        ef.ChannelMessageEnd("AAD");

        // Authenticated data *must* be pushed before
        //  Confidential/Authenticated data. Otherwise
        //  we must catch the BadState exception
        ef.ChannelPut( "", (const byte*)plaintext.data(), plaintext.size() );
        ef.ChannelMessageEnd("");

        // Pretty print
        StringSource( cipher, true,
                      new HexEncoder( new StringSink( encoded ), true, 16, " " ) );
    }
    catch( CryptoPP::BufferedTransformation::NoChannelSupport& e )
    {
        // The tag must go in to the default channel:
        //  "unknown: this object doesn't support multiple channels"
        std::cerr << "Caught NoChannelSupport..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    catch( CryptoPP::AuthenticatedSymmetricCipher::BadState& e )
    {
        // Pushing plaintext before ADATA results in:
        //  "GMC/AES: Update was called before State_IVSet"
        std::cerr << "Caught BadState..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    catch( CryptoPP::InvalidArgument& e )
    {
        std::cerr << "Caught InvalidArgument..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    //std::cout << "cipher: " << cipher.size() << std::endl;
    //std::cout << "encoded: " << encoded.size() << std::endl;
    //std::cout << "plaintext: " << plaintext.size() << std::endl;

    /*********************************\
    \*********************************/

    // Attack the first and last byte
    //if( cipher.size() > 1 )
    //{
    //  cipher[ 0 ] |= 0x0F;
    //  cipher[ cipher.size()-1 ] |= 0x0F;
    //}

    /*********************************\
    \*********************************/
    //std::cout << "cipher:" << cipher << std::endl;
    //std::cout << std::endl;
    return cipher;
}

// decrypts with aes cbc
std::string crypt::decrypt_cbc(std::string ciphertext)
{
    std::string plaintext;
    std::cout << std::endl << std::endl;
    //
    // Decrypt
    //
    CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::MAX_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

    CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( plaintext ) );
    stfDecryptor.Put( reinterpret_cast<const unsigned char*>( ciphertext.c_str() ), ciphertext.size() );
    stfDecryptor.MessageEnd();
    //
    // Dump Decrypted Text
    //
    std::cout << "Decrypted Text: " << std::endl;
    std::cout << plaintext;
    std::cout << std::endl << std::endl;
    return plaintext;
}


//decrypts with aes gcm
//this is broken I am going to be defaulting to only using cbc for now but planning to revist this later.
std::string crypt::decrypt_gcm(std::string cipher){
    //std::cout << "------------decryption----------" << std::endl;

    std::string adata, pdata, buff;


    // Recovered
    std::string radata, rpdata;
    try
    {
        GCM< AES >::Decryption d;
        d.SetKeyWithIV( key, sizeof(key), iv, sizeof(iv) );

        // Break the cipher text out into it's
        //  components: Encrypted Data and MAC Value
        std::string enc = cipher.substr( 0, cipher.length()-TAG_SIZE );
        std::string mac = cipher.substr( cipher.length()-TAG_SIZE );

        //std::cout << "enc: " << enc << std::endl;
        //std::cout << "mac: " << mac << std::endl;
        //std::cout << "cipher: " << cipher << std::endl;

        // Sanity checks
        //std::cout << "cipher-size:" << cipher.size() << " enc-size:" << enc.size() << " mac-size:" << mac.size() << std::endl;
        assert( cipher.size() == enc.size() + mac.size() );
        //assert( enc.size() == pdata.size() );
        assert( TAG_SIZE == mac.size() );

        // Not recovered - sent via clear channel
        radata = adata;

        // Object will not throw an exception
        //  during decryption\verification _if_
        //  verification fails.
        //AuthenticatedDecryptionFilter df( d, NULL,
        //                                  AuthenticatedDecryptionFilter::MAC_AT_BEGIN );

        AuthenticatedDecryptionFilter df( d, NULL,
                                          AuthenticatedDecryptionFilter::MAC_AT_BEGIN |
                                          AuthenticatedDecryptionFilter::THROW_EXCEPTION, TAG_SIZE );
        //std::cout << "check0" << std::endl;

        // The order of the following calls are important
        df.ChannelPut( "", (const byte*)mac.data(), mac.size() );
        df.ChannelPut( "AAD", (const byte*)"wootywoot", 9 );
        df.ChannelPut( "", (const byte*)enc.data(), enc.size() );

        //std::cout << "check0.25" << std::endl;

        // If the object throws, it will most likely occur
        //  during ChannelMessageEnd()
        df.ChannelMessageEnd( "AAD" );
        //std::cout << "check0.35" << std::endl;
        df.ChannelMessageEnd( "" );
        //std::cout << "check0.4" << std::endl;

        // If the object does not throw, here's the only
        //  opportunity to check the data's integrity
        //std::cout << "check0.5" << std::endl;
        bool b = false;
        b = df.GetLastResult();
        assert( true == b );
        //std::cout << "check1" << std::endl;

        // Remove data from channel
        std::string retrieved;
        size_t n = (size_t)-1;

        // Plain text recovered from enc.data()
        df.SetRetrievalChannel( "" );
        n = (size_t)df.MaxRetrievable();
        //std::cout << n << std::endl;
        retrieved.resize( n );

        if( n > 0 ) { df.Get( (byte*)retrieved.data(), n ); }
        rpdata = retrieved;
        //assert( rpdata == pdata );

        // Hmmm... No way to get the calculated MAC
        //  mac out of the Decryptor/Verifier. At
        //  least it is purported to be good.
        df.SetRetrievalChannel( "AAD" );
        n = (size_t)df.MaxRetrievable();
        //std::cout << n << std::endl;
        retrieved.resize( n );

        if( n > 0 ) { df.Get( (byte*)retrieved.data(), n ); }
        radata = retrieved;
        //assert( retrieved == mac );

        // All is well - work with data
        //std::cout << "Decrypted and Verified data. Ready for use." << std::endl;
        //std::cout << std::endl;

        //std::cout << "adata length: " << adata.size() << std::endl;
        //std::cout << "pdata length: " << pdata.size() << std::endl;
        //std::cout << std::endl;

        //std::cout << "adata: " << adata << std::endl;
        //std::cout << "pdata: " << pdata << std::endl;
        //std::cout << std::endl;

        //std::cout << "cipher text: " << std::endl << " " << cipher << std::endl;
        //std::cout << std::endl;

        //std::cout << "recovered adata length: " << adata.size() << std::endl;
        //std::cout << "recovered pdata length: " << rpdata.size() << std::endl;
        //std::cout << std::endl;

        //std::cout << "recovered adata: " << radata << std::endl;
        //std::cout << "recovered pdata: " << rpdata << std::endl;
        //std::cout << std::endl;
    }
    catch( CryptoPP::InvalidArgument& e )
    {
        std::cerr << "Caught InvalidArgument..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    catch( CryptoPP::AuthenticatedSymmetricCipher::BadState& e )
    {
        // Pushing PDATA before ADATA results in:
        //  "GMC/AES: Update was called before State_IVSet"
        std::cerr << "Caught BadState..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    catch( CryptoPP::HashVerificationFilter::HashVerificationFailed& e )
    {
        std::cerr << "Caught HashVerificationFailed..." << std::endl;
        std::cerr << e.what() << std::endl;
        std::cerr << std::endl;
    }
    return rpdata;
}

////////////////////////////////
//// file funcs
////////////////////////////////

// gets the size of a file
long get_file_size(std::string fp)
{
    struct stat stat_buf;
    int rc = stat(fp.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

//encyrpts a file
void crypt::encrypt_file(std::string src, std::string dest) {
    std::string temp;
    long goal, prog = 0;
    std::string str, buff, test;

    //where to write the encrypted data to
    std::ofstream ofp(dest);

    //where to read the encrypted data from
    CryptoPP::FileSource fp(src.c_str(), false, new CryptoPP::StringSink(str));
    goal = get_file_size(src);

    // write iv to file first
    temp = get_iv_as_string();
    ofp.write(temp.data(), AES::BLOCKSIZE);

    //write salt to file
    ofp.write(salt, BCRYPT_HASHSIZE);

    //encrypts file in chunks
    while(prog < goal){
        fp.Pump(FILE_READ_SIZE);
        prog = prog + FILE_READ_SIZE;
        buff = encrypt_gcm(str);
        ofp.write(buff.data(), buff.size());
        // del str so that im not eating ram
        // has to be a better way to do this ~('_')~
        str  = "";
    }
    ofp.close();
}

//decrypts a file
void crypt::decrypt_file(std::string src, std::string dest, std::string key, crypt* cryptobj){
    int i;
    long goal, prog = 0;
    std::string str, buff;

    //where to write decrypted data to
    std::ofstream ofp(dest);

    //where to read encrypted data from
    CryptoPP::FileSource fp(src.c_str(), false, new CryptoPP::StringSink(str));
    goal = get_file_size(src);

    //get iv from file
    fp.Pump(AES::BLOCKSIZE);
    set_iv_from_string(str);
    str = "";

    //get salt from file
    fp.Pump(BCRYPT_HASHSIZE);
    for (i = 0; i < BCRYPT_HASHSIZE; i++){
        salt[i] = str.c_str()[i];
    }

    // setkey
    cryptobj->setkey(key);

    str = "";

    //decrypts file in chunks
    while(prog < goal){
        // the + 16 is required otherwise the decrypt will not work
        fp.Pump(FILE_READ_SIZE + 16);
        prog = prog + FILE_READ_SIZE + 16;
        buff = decrypt_gcm(str);
        ofp.write(buff.data(), buff.size());
        // del str so that im not eating ram
        // has to be a better way to do this ~('_')~
        str  = "";
    }
}


////////////////////////////////
//// Crypt Ctypes Wrapper funcs
////////////////////////////////
//sets a new key
void encrypt_file(const char *new_key, const char* path, const char* dest){
    crypt cryptobj;
    std::string temp;
    //convert const char* to string
    std::string skey = new_key;
    std::string spath = path;
    std::string sdest = dest;
    //set key
    cryptobj.newkey(skey);
    //encrypt file here

    cryptobj.encrypt_file(spath, sdest);
}

void decrypt_file(const char *new_key, const char* path, const char* dest){
    crypt cryptobj;
    //convert const char* to string
    std::string skey = new_key;
    std::string spath = path;
    std::string sdest = dest;
    //encrypt file here

    cryptobj.decrypt_file(spath, sdest, skey, &cryptobj);
}

void free_string(const char* targ){
    delete targ;
}