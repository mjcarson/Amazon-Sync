from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^exit$', views.exit, name='exit'),
    url(r'^setup$', views.setup, name='setup'),
    url(r'^get_setup$', views.get_setup, name='get_setup'),
    url(r'^new_dir$', views.new_dir, name='new_dir'),
    url(r'^get_new_dir$', views.get_new_dir, name='get_new_dir'),
    url(r'^download_file$', views.download_file, name='download_file'),
    url(r'^style.css$', views.style, name="style.css")
]