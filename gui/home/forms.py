from django import forms

class Setup_Form(forms.Form):
    key = forms.CharField(label='key', max_length=100)
    staging = forms.CharField(label='staging', max_length=400)

class Add_Dir_Form(forms.Form):
    name = forms.CharField(label='name', max_length=100)
    path = forms.CharField(label='path', max_length=400)

class Download_File_Form(forms.Form):
    share = forms.CharField(label='share', max_length=100)
    file = forms.CharField(label='file', max_length=100)
    path = forms.CharField(label='path', max_length=400)
    version = forms.CharField(label='version', max_length=100)
    enc_id = forms.CharField(label='path', max_length=400)