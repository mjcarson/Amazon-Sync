from django.shortcuts import render, redirect
from .forms import Setup_Form, Add_Dir_Form, Download_File_Form
import socket
import json
import time
# Create your views here.
#from django.http import HttpResponse

def __conect():
    """Connects to the local amazon-sync"""
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn.connect(("127.0.0.1", 8099))
    return conn

def __tojson(packed):
    """converts reg string to a json"""
    version_unpacked = {}
    # unpack the json
    unpacked = json.loads(packed)
    for file in unpacked:
        version_unpacked[file] = json.loads(unpacked[file])
        for version in version_unpacked[file]:
            version_unpacked[file][version] = json.loads(version_unpacked[file][version])
    # prints json nicely
    print(json.dumps(version_unpacked, indent=4))
    return version_unpacked

def home(request):
    print("home!")
    # connect to worker
    conn = __conect()
    # send get reg command
    conn.send("get_reg".encode())
    # get length
    leng = conn.recv(4096)
    leng = int(leng.decode())
    # get reg json
    packed = conn.recv(leng)
    # check if setup is complete
    if packed.decode() != "setup":
        unpacked = __tojson(packed)
        context = {'title':'Amazon-Sync', "unpacked": unpacked}
        return render(request, 'home/home.html', context)
    else:
        context = {'title':'Setup Amazon-Sync'}
        return render(request, 'home/setup.html', context)

def get_setup(request):
    """Gets setup from setup"""
    # make sure reuest is a post
    if request.method == 'POST':
        form = Setup_Form(request.POST)
        # ensure form is valid
        if form.is_valid():
            key = form.cleaned_data['key']
            staging = form.cleaned_data['staging']
            # connect to amazon-sync
            conn = __conect()
            # send key
            conn.send("setup".encode())
            time.sleep(.1)
            conn.send(key.encode())
            time.sleep(.1)
            # send staging
            conn.send(staging.encode())
            return redirect("/")
    # something went wrong back to setup page
    context = {'title': 'Setup Amazon-Sync'}
    return render(request, 'home/setup.html', context)



def setup(request):
    print("setup!")
    context = {'title': 'Setup Amazon-Sync'}
    return render(request, 'home/setup.html', context)

def new_dir(request):
    print("new dir!")
    context = {'title': 'Add a directory'}
    return render(request, 'home/new_dir.html', context)

def get_new_dir(request):
    """Gets path from new_dir"""
    # make sure reuest is a post
    if request.method == 'POST':
        form = Add_Dir_Form(request.POST)
        # ensure form is valid
        if form.is_valid():
            name = form.cleaned_data['name']
            path = form.cleaned_data['path']
            # connect to amazon-sync
            conn = __conect()
            # send key
            conn.send("add_dir".encode())
            time.sleep(.1)
            conn.send(name.encode())
            time.sleep(.1)
            conn.send(path.encode())
            return redirect("/")
    # something went wrong back to setup page
    context = {'title': 'Amazon-Sync'}
    return render(request, 'home/home.html', context)

def download_file(request):
    """Gets path from new_dir"""
    # make sure reuest is a post
    if request.method == 'POST':
        form = Download_File_Form(request.POST)
        # ensure form is valid
        if form.is_valid():
            share = form.cleaned_data['share']
            file = form.cleaned_data['file']
            path = form.cleaned_data['path']
            version = form.cleaned_data['version']
            encrypted_id = form.cleaned_data['enc_id']
            # connect to amazon-sync
            conn = __conect()
            # send key
            conn.send("download_file".encode())
            time.sleep(.1)
            conn.send(share.encode())
            time.sleep(.1)
            conn.send(file.encode())
            time.sleep(.1)
            conn.send(path.encode())
            time.sleep(.1)
            conn.send(version.encode())
            time.sleep(.1)
            conn.send(encrypted_id.encode())
            return redirect("/")
    # something went wrong back to setup page
    context = {'title': 'Amazon-Sync'}
    return render(request, 'home/home.html', context)

def style(request):
    """Serves style sheet"""
    context = {'title': 'Amazon-Sync'}
    return render(request, 'home/style.css', context)

def exit(request):
    print("exit!")
    # connect to amazon-sync
    conn = __conect()
    # send get reg command
    conn.send("shutdown".encode())
    context = {'title':'Exiting Amazon-Sync'}
    return render(request, 'home/exit.html', context)
